import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '@app/shared/service/employee.service';
import {Employee} from '@app/shared/models/employee';
import {Router} from '@angular/router';
import {Almacen} from '@app/shared/models/almacen';
import {AlmacenService} from '@app/shared/service/almacen.service';

@Component({
  selector: 'app-almacen',
  templateUrl: './almacen.component.html',
  styleUrls: ['./almacen.component.scss']
})
export class AlmacenComponent implements OnInit {

  public almacens: Almacen[];

  constructor(private almacenService: AlmacenService,
              private router: Router) {
    this.almacens = [];
  }

  ngOnInit() {
    this.getAlmacensList();
  }

  editAlmacen(almacen: Almacen) {
    this.router.navigate(['/almacen/edit', almacen.id]);
  }

  deleteAlmacen(id: number) {
    this.almacenService.deleteAlmacen(id).subscribe(value => {
      this.getAlmacensList();
    });
  }

  private getAlmacensList() {
    this.almacenService.getAlmacens().subscribe(value => {
      this.almacens = value;
    });
  }

}
