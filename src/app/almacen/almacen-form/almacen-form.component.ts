import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AlmacenService} from '@app/shared/service/almacen.service';
import {Almacen} from '@app/shared/models/almacen';

@Component({
  selector: 'app-almacen-form',
  templateUrl: './almacen-form.component.html',
  styleUrls: ['./almacen-form.component.scss']
})
export class AlmacenFormComponent implements OnInit {

  almacen: Almacen;

  constructor(private almacenService: AlmacenService,
              private route: ActivatedRoute,
              private router: Router) {
    this.almacen = new Almacen();
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.almacenService.createAlmacen(this.almacen).subscribe(value => {
      this.router.navigate(['/almacen']);
    });
  }

}
