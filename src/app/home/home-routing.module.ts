import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {Route, extract} from '@app/core';
import {HomeComponent} from './home.component';
import {DepartamentComponent} from '@app/departament/departament.component';
import {DepartmentFormComponent} from '@app/departament/department-form.component';
import {EmployeeComponent} from '@app/employee/employee.component';
import {EmployeeFormComponent} from '@app/employee/employee-form/employee-form.component';
import {AlmacenComponent} from '@app/almacen/almacen.component';
import {AlmacenFormComponent} from '@app/almacen/almacen-form/almacen-form.component';
import {ObraComponent} from '@app/obra/obra.component';
import {ObraFormComponent} from '@app/obra/obra-form.component';
import {EtapaComponent} from '@app/etapa/etapa.component';
import {EtapaFormComponent} from '@app/etapa/etapa-form/etapa-form.component';
import {ProfileFormComponent} from '@app/employee/profile-form/profile-form.component';
import {ContractFormComponent} from '@app/employee/contract-form/contract-form.component';
import {CargoComponent} from '@app/cargo/cargo.component';
import {CargoFormComponent} from '@app/cargo/cargo-form/cargo-form.component';
import {TypeContractComponent} from '@app/type-contract/type-contract.component';
import {TypeContractFormComponent} from '@app/type-contract/type-contract-form/type-contract-form.component';
import {TipoEquipoComponent} from '@app/equipo/tipo-equipo/tipo-equipo.component';
import {TipoEquipoFormComponent} from '@app/equipo/tipo-equipo-form/tipo-equipo-form.component';
import {EnployeeObraComponent} from '@app/obra/enployee-obra/enployee-obra.component';
import {ObraEtapaComponent} from '@app/obra/obra-etapa/obra-etapa.component';
import {EquipoComponent} from '@app/equipo/equipo.component';
import {EquipoFormComponent} from '@app/equipo/equipo-form/equipo-form.component';
import {EmployeeEquipoComponent} from '@app/employee/employee-equipo/employee-equipo.component';
import {TypeAccidentComponent} from '@app/accident/type-accident/type-accident.component';
import {AccidentComponent} from '@app/accident/accident.component';

const routes: Routes = [
  Route.withShell([
    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: 'departament', component: DepartamentComponent},
    {path: 'departament/create', component: DepartmentFormComponent},
    {path: 'departament/edit/:id', component: DepartmentFormComponent},
    {path: 'cargo', component: CargoComponent},
    {path: 'cargo/create', component: CargoFormComponent},
    {path: 'cargo/edit/:id', component: CargoFormComponent},
    {path: 'typecontract', component: TypeContractComponent},
    {path: 'typecontract/create', component: TypeContractFormComponent},
    {path: 'typecontract/edit/:id', component: TypeContractFormComponent},
    {path: 'employee', component: EmployeeComponent},
    {path: 'employee/create', component: EmployeeFormComponent},
    {path: 'employee/edit/:id', component: EmployeeFormComponent},
    {path: 'employee/profile/create', component: ProfileFormComponent},
    {path: 'employee/contract/create', component: ContractFormComponent},

    {path: 'employee/obra/create', component: EnployeeObraComponent},
    {path: 'obra/etapa/create', component: ObraEtapaComponent},

    {path: 'equipo', component: EquipoComponent},
    {path: 'equipo/create', component: EquipoFormComponent},
    {path: 'equipo/edit/:id', component: EquipoFormComponent},
    {path: 'tipoequipo', component: TipoEquipoComponent},
    {path: 'tipoequipo/create', component: TipoEquipoFormComponent},
    {path: 'tipoequipo/edit/:id', component: TipoEquipoFormComponent},

    {path: 'employee/equipo/create', component: EmployeeEquipoComponent},

    {path: 'accident/type', component: TypeAccidentComponent},
    {path: 'accident', component: AccidentComponent},
    {path: 'accident/create', component: AccidentComponent},

    {path: 'almacen', component: AlmacenComponent},
    {path: 'almacen/create', component: AlmacenFormComponent},
    {path: 'almacen/edit/:id', component: AlmacenFormComponent},
    {path: 'obra', component: ObraComponent},
    {path: 'obra/create', component: ObraFormComponent},
    {path: 'obra/edit/:id', component: ObraFormComponent},
    {path: 'etapa', component: EtapaComponent},
    {path: 'etapa/create', component: EtapaFormComponent},
    {path: 'etapa/edit/:id', component: EtapaFormComponent},
    {path: 'home', component: HomeComponent, data: {title: extract('Home')}}
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class HomeRoutingModule {
}
