import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {GenericService} from '@app/shared/service/generic-service';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ObraEtapaService extends GenericService {

  constructor(http: HttpClient) {
    super(http, 'obraEtapas/');
  }

  getObraEtapaByObra(id: number): Observable<any> {
    return this.http.get(this.url + 'obra/' + id);
  }

}
