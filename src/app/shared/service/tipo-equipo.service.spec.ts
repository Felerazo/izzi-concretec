import { TestBed, inject } from '@angular/core/testing';

import { TipoEquipoService } from './tipo-equipo.service';

describe('TipoEquipoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TipoEquipoService]
    });
  });

  it('should be created', inject([TipoEquipoService], (service: TipoEquipoService) => {
    expect(service).toBeTruthy();
  }));
});
