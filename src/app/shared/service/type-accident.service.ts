import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GenericService} from '@app/shared/service/generic-service';

@Injectable()
export class TypeAccidentService extends GenericService{

  constructor(http: HttpClient) {
    super(http, 'typeAccidents/');
  }

}
