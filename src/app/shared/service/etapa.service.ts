import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {Etapa} from '@app/shared/models/etapa';
import {GenericService} from '@app/shared/service/generic-service';

@Injectable()
export class EtapaService extends GenericService {

  constructor(http: HttpClient) {
    super(http, 'etapas/');
  }
}
