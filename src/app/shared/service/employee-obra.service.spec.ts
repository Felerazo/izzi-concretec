import { TestBed, inject } from '@angular/core/testing';

import { EmployeeObraService } from './employee-obra.service';

describe('EmployeeObraService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmployeeObraService]
    });
  });

  it('should be created', inject([EmployeeObraService], (service: EmployeeObraService) => {
    expect(service).toBeTruthy();
  }));
});
