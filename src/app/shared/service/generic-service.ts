import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

export class GenericService {

  protected url: string;
  protected http: HttpClient;
  constructor(http: HttpClient, url: string) {
    this.http = http;
    this.url = url;
  }

  getEntities(): Observable<any> {
    return this.http.get(this.url);
  }

  createEntity(entity: any): Observable<any> {
    return this.http.post(this.url, entity);
  }

  updateEntity(entity: any): Observable<any> {
    return this.http.put(this.url, entity);
  }

  getEntity(id: number): Observable<any> {
    return this.http.get(this.url + id);
  }

  deleteEntity(id: number): Observable<any> {
    return this.http.delete(this.url + id);
  }
}
