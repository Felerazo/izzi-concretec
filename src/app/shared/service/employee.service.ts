import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Departament} from '@app/shared/models/departament';
import {HttpClient} from '@angular/common/http';
import {Employee} from '@app/shared/models/employee';

@Injectable()
export class EmployeeService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = 'employees/';
  }

  getEmployees(): Observable<any> {
    return this.http.get(this.url);
  }

  getEmployeesFilter(depertamentId: number): Observable<any> {
    return this.http.get(this.url + '?query=' + depertamentId);
  }

  createEmployee(employee: Employee): Observable<any> {
    return this.http.post(this.url, employee);
  }

  getEmployee(id: number): Observable<any> {
    return this.http.get(this.url + id);
  }

  deleteEmployee(id: number): Observable<any> {
    return this.http.delete(this.url + id);
  }

}
