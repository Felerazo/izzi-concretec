import { TestBed, inject } from '@angular/core/testing';

import { TypeAccidentService } from './type-accident.service';

describe('TypeAccidentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TypeAccidentService]
    });
  });

  it('should be created', inject([TypeAccidentService], (service: TypeAccidentService) => {
    expect(service).toBeTruthy();
  }));
});
