import { TestBed, inject } from '@angular/core/testing';

import { EmloyeeEquipoService } from './emloyee-equipo.service';

describe('EmloyeeEquipoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmloyeeEquipoService]
    });
  });

  it('should be created', inject([EmloyeeEquipoService], (service: EmloyeeEquipoService) => {
    expect(service).toBeTruthy();
  }));
});
