import {Injectable} from '@angular/core';
import {GenericService} from '@app/shared/service/generic-service';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class CargoService extends GenericService {

  constructor(http: HttpClient) {
    super(http, 'cargos/');
  }
}
