import { TestBed, inject } from '@angular/core/testing';

import { ObraEtapaService } from './obra-etapa.service';

describe('ObraEtapaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ObraEtapaService]
    });
  });

  it('should be created', inject([ObraEtapaService], (service: ObraEtapaService) => {
    expect(service).toBeTruthy();
  }));
});
