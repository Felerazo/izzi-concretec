import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GenericService} from '@app/shared/service/generic-service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class EmloyeeEquipoService extends GenericService{

  constructor(http: HttpClient) {
    super(http, 'employeeEquipos/');
  }

  getEmployeeEquipoByEmployee(id: number): Observable<any> {
    return this.http.get(this.url + 'employee/' + id);
  }

}
