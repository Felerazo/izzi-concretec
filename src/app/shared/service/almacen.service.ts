import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {Almacen} from '@app/shared/models/almacen';

@Injectable()
export class AlmacenService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = 'almacenes/';
  }

  getAlmacens(): Observable<any> {
    return this.http.get(this.url);
  }

  createAlmacen(employee: Almacen): Observable<any> {
    return this.http.post(this.url, employee);
  }

  deleteAlmacen(id: number): Observable<any> {
    return this.http.delete(this.url + id);
  }

}
