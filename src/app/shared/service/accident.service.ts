import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GenericService} from '@app/shared/service/generic-service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AccidentService extends GenericService{

  constructor(http: HttpClient) {
    super(http, 'accidents/');
  }

  getEntitiesFilter(type: number): Observable<any> {
    return this.http.get(this.url + '?tipo=' + type);
  }
}
