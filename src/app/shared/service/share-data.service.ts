import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Employee} from '@app/shared/models/employee';
import {Observable} from 'rxjs/Observable';
import {Obra} from '@app/shared/models/obra';

@Injectable()
export class ShareDataService {

  private employee = new BehaviorSubject<Employee>(null);
  private obra = new BehaviorSubject<Obra>(null);
  constructor() { }

  getEmployee(): Employee {
    return this.employee.getValue();
  }

  getEmployeeSubs$(): Observable<Employee> {
    return this.employee.asObservable();
  }

  setEmployeeSubs$(value: Employee) {
    this.employee.next(value);
  }

  getObra(): Obra {
    return this.obra.getValue();
  }

  getObraSubs$(): Observable<Obra> {
    return this.obra.asObservable();
  }

  setObraSubs$(value: Obra) {
    this.obra.next(value);
  }

}
