import { TestBed, inject } from '@angular/core/testing';

import { ProfileEmployeeService } from './profile-employee.service';

describe('ProfileEmployeeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfileEmployeeService]
    });
  });

  it('should be created', inject([ProfileEmployeeService], (service: ProfileEmployeeService) => {
    expect(service).toBeTruthy();
  }));
});
