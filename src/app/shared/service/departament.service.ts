import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GenericService} from '@app/shared/service/generic-service';

@Injectable()
export class DepartamentService extends GenericService {


  constructor(http: HttpClient) {
    super(http, 'departamentos/');
  }
}
