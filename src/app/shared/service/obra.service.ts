import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {Almacen} from '@app/shared/models/almacen';
import {Obra} from '@app/shared/models/obra';
import {GenericService} from '@app/shared/service/generic-service';

@Injectable()
export class ObraService extends GenericService {

  constructor(http: HttpClient) {
    super(http, 'obras/');
  }

  getObrasFilter(obra: Obra): Observable<any> {
    return this.http.get(this.url + '?fechaInicio=' + obra.fecha_inicio + '&' + 'fechaFin=' + obra.fecha_fin);
  }

}
