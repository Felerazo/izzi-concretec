import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GenericService} from '@app/shared/service/generic-service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class EquipoService extends GenericService{

  constructor(http: HttpClient) {
    super(http, 'equipos/');
  }

  getEntitiesFilter(type: number): Observable<any> {
    return this.http.get(this.url + '?tipo=' + type);
  }

}
