import { Injectable } from '@angular/core';
import {GenericService} from '@app/shared/service/generic-service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class EmployeeObraService extends GenericService {

  constructor(http: HttpClient) {
    super(http, 'employeesobras/');
  }

  getEmployeeObraByEmployee(id: number): Observable<any> {
    return this.http.get(this.url + 'employee/' + id);
  }

}
