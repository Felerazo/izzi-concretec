export class Obra {

  id: number;
  code: string;
  nombre: string;
  descripcion: Date;
  fecha_inicio: Date;
  fecha_fin: string;
  ubicacion: string;
}
