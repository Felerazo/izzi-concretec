import {Departament} from '@app/shared/models/departament';
import {Cargo} from '@app/shared/models/cargo';

export class Employee {

  id: number;
  firstName: string;
  lastName: string;
  ci: number;
  telefono: number;
  celular: number;
  fechaNacimiento: string;
  estadoCivil: string;
  direccion: string;
  segundaDireccion: string;
  departamentId: Departament;
  cargoId: Cargo;
}
