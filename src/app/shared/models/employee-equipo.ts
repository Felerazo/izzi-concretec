export class EmployeeEquipo {
  id: number;
  employeeId: number;
  equipoId: number;
  description: string;
  equipoName: string;
  equipoDescription: string;
}
