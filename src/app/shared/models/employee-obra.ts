export class EmployeeObra {

  id: number;
  employeeId: number;
  obraId: number;
  nameObra: string;
  description: string;
}
