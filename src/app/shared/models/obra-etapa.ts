export class ObraEtapa {
  id: number;
  etapaId: number;
  obraId: number;
  description: string;
  status: string;
  etapaName: string;
}
