export class Equipo {
  id: number;
  name: string;
  code: string;
  description: string;
  tipoEquipoName: string;
  tipoEquipoId: number;
}
