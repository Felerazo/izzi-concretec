export class Profile {
  experiencia: number;
  formacion: string;
  especializacion: string;
  sector: string;
  id: number;
  employeeId: number;
}
