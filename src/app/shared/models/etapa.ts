import {Employee} from '@app/shared/models/employee';

export class Etapa {

  id: number;
  code: string;
  nombre: string;
  fechaInicio: string;
  fechaFin: string;
  nroEmpleados: number;
}
