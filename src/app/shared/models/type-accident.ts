export class TypeAccident {
  id: number;
  name: string;
  description: string;
}
