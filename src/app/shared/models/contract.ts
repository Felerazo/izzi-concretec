export class Contract {
  id: number;
  employeeId: number;
  tipoContractId: number;
  initDate: string;
  endDate: string;
  description: string;
}
