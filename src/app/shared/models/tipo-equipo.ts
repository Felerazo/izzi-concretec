export class TipoEquipo {

  id: number;
  code: string;
  nombre_equipo: string;
  nombre_maquina: string;
}
