export class Accident {
  id: number;
  employeeId: number;
  typeAccidentId: number;
  description: string;
  employeeData: string;
  typeAccidentData: string;
}
