import {Component, OnInit} from '@angular/core';
import {AccidentService} from '@app/shared/service/accident.service';
import {TypeAccidentService} from '@app/shared/service/type-accident.service';
import {EmployeeService} from '@app/shared/service/employee.service';
import {TypeAccident} from '@app/shared/models/type-accident';
import {Employee} from '@app/shared/models/employee';
import {Accident} from '@app/shared/models/accident';

@Component({
  selector: 'app-accident',
  templateUrl: './accident.component.html',
  styleUrls: ['./accident.component.scss']
})
export class AccidentComponent implements OnInit {

  typeAccidents: TypeAccident[];
  employees: Employee[];
  accidents: Accident[];
  accident: Accident;
  isCollapsed: boolean;
  isCollapsedFilter: boolean;
  typeAccidentFilter: number;

  constructor(private accidentService: AccidentService,
              private typeAccidentService: TypeAccidentService,
              private employeeService: EmployeeService) {
    this.typeAccidents = [];
    this.employees = [];
    this.accidents = [];
    this.accident = new Accident();
    this.isCollapsed = true;
    this.isCollapsedFilter = true;
  }

  ngOnInit() {
    this.getEmployees();
    this.getTypeAccidents();
    this.getAccidents();
  }

  onSubmit(): void {
    this.accidentService.createEntity(this.accident).subscribe(value => {
      this.accident = new Accident();
      this.getAccidents();
    });
    this.isCollapsed = true;
  }

  public editAccident(accident: Accident): void {
    this.collapseForm()
    this.accident = accident;
  }

  private getEmployees() {
    this.employeeService.getEmployees().subscribe(value => {
      this.employees = value;
    });
  }

  private getTypeAccidents() {
    this.typeAccidentService.getEntities().subscribe(value => {
      this.typeAccidents = value;
    });
  }

  private getAccidents() {
    this.accidentService.getEntities().subscribe(value => {
      this.accidents = value;
    });
  }

  public getFilterAccidentes() {
    this.accidentService.getEntitiesFilter(this.typeAccidentFilter).subscribe(value => {
      this.accidents = value;
    });
  }

  public collapseForm() {
    this.isCollapsed = !this.isCollapsed;
    if (!this.isCollapsed && !this.isCollapsedFilter) {
      this.isCollapsedFilter = true;
    }
  }

  public collapseFormFilter() {
    this.isCollapsedFilter = !this.isCollapsedFilter;
    if (!this.isCollapsed && !this.isCollapsedFilter) {
      this.isCollapsed = true;
    }
  }

}
