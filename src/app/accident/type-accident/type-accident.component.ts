import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ShareDataService} from '@app/shared/service/share-data.service';
import {TypeAccident} from '@app/shared/models/type-accident';
import {TypeAccidentService} from '@app/shared/service/type-accident.service';

@Component({
  selector: 'app-type-accident',
  templateUrl: './type-accident.component.html',
  styleUrls: ['./type-accident.component.scss']
})
export class TypeAccidentComponent implements OnInit {

  typeAccident: TypeAccident;
  typeAccidents: TypeAccident[];

  constructor(private shareDataService: ShareDataService,
              private typeAccidentService: TypeAccidentService,
              private router: Router) {
    this.typeAccident = new TypeAccident();
    this.typeAccidents = [];
  }

  ngOnInit() {
    this.getTypeAccidents();
  }

  onSubmit(): void {
    this.typeAccidentService.createEntity(this.typeAccident).subscribe(value => {
      this.typeAccident = new TypeAccident();
      this.getTypeAccidents();
    });
  }

  editTypeAccident(typeAccident: TypeAccident) {
    this.typeAccident = typeAccident;
  }

  private getTypeAccidents() {
    this.typeAccidentService.getEntities().subscribe(value => {
      this.typeAccidents = value;
    });
  }

}
