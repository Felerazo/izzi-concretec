import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';
import {NgbDateAdapter, NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {CoreModule} from '@app/core';
import {SharedModule} from '@app/shared';
import {HomeModule} from './home/home.module';
import {AboutModule} from './about/about.module';
import {LoginModule} from './login/login.module';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {DepartamentComponent} from './departament/departament.component';
import {DepartamentService} from '@app/shared/service/departament.service';
import {DepartmentFormComponent} from './departament/department-form.component';
import {EmployeeComponent} from './employee/employee.component';
import {EmployeeService} from '@app/shared/service/employee.service';
import {EmployeeFormComponent} from './employee/employee-form/employee-form.component';
import {AlmacenComponent} from './almacen/almacen.component';
import {AlmacenFormComponent} from './almacen/almacen-form/almacen-form.component';
import {AlmacenService} from '@app/shared/service/almacen.service';
import { ObraComponent } from './obra/obra.component';
import { ObraFormComponent } from './obra/obra-form.component';
import {ObraService} from '@app/shared/service/obra.service';
import {MDataTableModule} from 'mdata-table';
import {EtapaService} from '@app/shared/service/etapa.service';
import { EtapaComponent } from './etapa/etapa.component';
import { EtapaFormComponent } from './etapa/etapa-form/etapa-form.component';
import { ProfileFormComponent } from './employee/profile-form/profile-form.component';
import { ContractFormComponent } from './employee/contract-form/contract-form.component';
import { CargoComponent } from './cargo/cargo.component';
import {CargoService} from '@app/shared/service/cargo.service';
import { CargoFormComponent } from './cargo/cargo-form/cargo-form.component';
import {TypeContractService} from '@app/shared/service/type-contract.service';
import { TypeContractComponent } from './type-contract/type-contract.component';
import { TypeContractFormComponent } from './type-contract/type-contract-form/type-contract-form.component';
import { TipoEquipoComponent } from './equipo/tipo-equipo/tipo-equipo.component';
import {TipoEquipoService} from '@app/shared/service/tipo-equipo.service';
import { TipoEquipoFormComponent } from './equipo/tipo-equipo-form/tipo-equipo-form.component';
import {ShareDataService} from '@app/shared/service/share-data.service';
import {ProfileEmployeeService} from '@app/shared/service/profile-employee.service';
import {ContractService} from '@app/shared/service/contract.service';
import { EnployeeObraComponent } from './obra/enployee-obra/enployee-obra.component';
import {EmployeeObraService} from '@app/shared/service/employee-obra.service';
import { ObraEtapaComponent } from './obra/obra-etapa/obra-etapa.component';
import {ObraEtapaService} from '@app/shared/service/obra-etapa.service';
import { EquipoComponent } from './equipo/equipo.component';
import {EquipoService} from '@app/shared/service/equipo.service';
import { EquipoFormComponent } from './equipo/equipo-form/equipo-form.component';
import { EmployeeEquipoComponent } from './employee/employee-equipo/employee-equipo.component';
import {EmloyeeEquipoService} from '@app/shared/service/emloyee-equipo.service';
import { AccidentComponent } from './accident/accident.component';
import { TypeAccidentComponent } from './accident/type-accident/type-accident.component';
import {TypeAccidentService} from '@app/shared/service/type-accident.service';
import {AccidentService} from '@app/shared/service/accident.service';
import {NgbDateNativeAdapter} from '@app/shared/service/date-picker-adapter.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    NgbModule.forRoot(),
    MDataTableModule.forRoot(),
    CoreModule,
    SharedModule,
    HomeModule,
    AboutModule,
    LoginModule,
    AppRoutingModule,
  ],
  declarations: [
    AppComponent,
    DepartamentComponent,
    DepartmentFormComponent,
    EmployeeComponent,
    EmployeeFormComponent,
    AlmacenComponent,
    AlmacenFormComponent,
    ObraComponent,
    ObraFormComponent,
    EtapaComponent,
    EtapaFormComponent,
    ProfileFormComponent,
    ContractFormComponent,
    CargoComponent,
    CargoFormComponent,
    TypeContractComponent,
    TypeContractFormComponent,
    TipoEquipoComponent,
    TipoEquipoFormComponent,
    EnployeeObraComponent,
    ObraEtapaComponent,
    EquipoComponent,
    EquipoFormComponent,
    EmployeeEquipoComponent,
    AccidentComponent,
    TypeAccidentComponent],
  providers: [
    NgbDateNativeAdapter,
    {provide: NgbDateAdapter, useClass: NgbDateNativeAdapter},
    DepartamentService,
    EmployeeService,
    AlmacenService,
    ObraService,
    EtapaService,
    CargoService,
    TypeContractService,
    TipoEquipoService,
    ShareDataService,
    ProfileEmployeeService,
    ContractService,
    EmployeeObraService,
    ObraEtapaService,
    EquipoService,
    EmloyeeEquipoService,
    TypeAccidentService,
    AccidentService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
