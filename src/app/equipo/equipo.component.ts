import { Component, OnInit } from '@angular/core';
import {EquipoService} from '@app/shared/service/equipo.service';
import {Router} from '@angular/router';
import {ShareDataService} from '@app/shared/service/share-data.service';
import {Equipo} from '@app/shared/models/equipo';
import {TipoEquipoService} from '@app/shared/service/tipo-equipo.service';
import {TipoEquipo} from '@app/shared/models/tipo-equipo';

@Component({
  selector: 'app-equipo',
  templateUrl: './equipo.component.html',
  styleUrls: ['./equipo.component.scss']
})
export class EquipoComponent implements OnInit {

  public equipos: Equipo[];
  tipoEquipos: TipoEquipo[];
  isCollapsedFilter: boolean;
  typeEquipoId: number;

  constructor(private equipoService: EquipoService,
              private tipoEquipoService: TipoEquipoService,
              private shareDataService: ShareDataService,
              private router: Router) {
    this.equipos = [];
    this.tipoEquipos = [];
    this.isCollapsedFilter = true;
  }

  ngOnInit() {
    this.getEquiposList();
    this.getTiposEquipos();
  }

  editEquipo(equipo: Equipo) {
    this.router.navigate(['/equipo/edit', equipo.id]);
  }

  addEquipo() {
    this.router.navigate(['equipo/create']);
  }

  private getEquiposList() {
    this.typeEquipoId = undefined;
    this.equipoService.getEntities().subscribe(value => {
      this.equipos = value;
    });
  }
  public getTiposEquipos() {
    this.tipoEquipoService.getEntities().subscribe(value => {
      this.tipoEquipos = value;
    });
  }
  public getEquiposFilter() {
    this.equipoService.getEntitiesFilter(this.typeEquipoId).subscribe(value => {
      this.equipos = value;
    });
  }

}
