import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Equipo} from '@app/shared/models/equipo';
import {EquipoService} from '@app/shared/service/equipo.service';
import {TipoEquipoService} from '@app/shared/service/tipo-equipo.service';
import {TipoEquipo} from '@app/shared/models/tipo-equipo';

@Component({
  selector: 'app-equipo-form',
  templateUrl: './equipo-form.component.html',
  styleUrls: ['./equipo-form.component.scss']
})
export class EquipoFormComponent implements OnInit {

  modeUpdate: boolean;
  equipo: Equipo;
  tipoEquipos: TipoEquipo[];

  constructor(private equipoService: EquipoService,
              private tipoEquipoService: TipoEquipoService,
              private route: ActivatedRoute,
              private router: Router) {
    this.modeUpdate = false;
    this.equipo = new Equipo();
    this.tipoEquipos = [];
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.modeUpdate = true;
        this.equipoService.getEntity(params['id']).subscribe(value => {
          this.equipo = value;
        });
      }
    });
    this.tipoEquipoService.getEntities().subscribe(value => {
      this.tipoEquipos = value;
    });
  }

  onSubmit() {
    if (this.modeUpdate) {
      this.equipoService.updateEntity(this.equipo).subscribe(value => {
        this.router.navigate(['/equipo']);
      });
    }
    if (!this.modeUpdate) {
      this.equipoService.createEntity(this.equipo).subscribe(value => {
        this.router.navigate(['/equipo']);
      });
    }
  }

}
