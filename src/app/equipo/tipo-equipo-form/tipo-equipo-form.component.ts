import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TipoEquipo} from '@app/shared/models/tipo-equipo';
import {TipoEquipoService} from '@app/shared/service/tipo-equipo.service';

@Component({
  selector: 'app-tipo-equipo-form',
  templateUrl: './tipo-equipo-form.component.html',
  styleUrls: ['./tipo-equipo-form.component.scss']
})
export class TipoEquipoFormComponent implements OnInit {

  modeUpdate: boolean;
  tipoEquipo: TipoEquipo;

  constructor(private tipoEquipoService: TipoEquipoService,
              private route: ActivatedRoute,
              private router: Router) {
    this.modeUpdate = false;
    this.tipoEquipo = new TipoEquipo();
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.modeUpdate = true;
        this.tipoEquipoService.getEntity(params['id']).subscribe(value => {
          this.tipoEquipo = value;
        });
      }
    });
  }

  onSubmit() {
    if (this.modeUpdate) {
      this.tipoEquipoService.updateEntity(this.tipoEquipo).subscribe(value => {
        this.router.navigate(['/tipoequipo']);
      });
    }
    if (!this.modeUpdate) {
      this.tipoEquipoService.createEntity(this.tipoEquipo).subscribe(value => {
        this.router.navigate(['/tipoequipo']);
      });
    }
  }

}
