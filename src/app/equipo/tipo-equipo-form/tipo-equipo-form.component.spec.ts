import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoEquipoFormComponent } from './tipo-equipo-form.component';

describe('TipoEquipoFormComponent', () => {
  let component: TipoEquipoFormComponent;
  let fixture: ComponentFixture<TipoEquipoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoEquipoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoEquipoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
