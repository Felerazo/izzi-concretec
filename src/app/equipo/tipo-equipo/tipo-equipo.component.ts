import { Component, OnInit } from '@angular/core';
import {TipoEquipoService} from '@app/shared/service/tipo-equipo.service';
import {Router} from '@angular/router';
import {TipoEquipo} from '@app/shared/models/tipo-equipo';

@Component({
  selector: 'app-tipo-equipo',
  templateUrl: './tipo-equipo.component.html',
  styleUrls: ['./tipo-equipo.component.scss']
})
export class TipoEquipoComponent implements OnInit {

  tipoEquipos: TipoEquipo[];
  constructor(private tipoEquipoService: TipoEquipoService,
              private router: Router) {
    this.tipoEquipos = [];
  }

  ngOnInit() {
    this.tipoEquipoService.getEntities().subscribe(value => {
      this.tipoEquipos = value;
    });
  }
  editTipoEquipo(tipoEquipo: TipoEquipo) {
    this.router.navigate(['/tipoequipo/edit', tipoEquipo.id]);
  }

}
