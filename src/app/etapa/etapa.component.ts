import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Etapa} from '@app/shared/models/etapa';
import {EtapaService} from '@app/shared/service/etapa.service';

@Component({
  selector: 'app-etapa',
  templateUrl: './etapa.component.html',
  styleUrls: ['./etapa.component.scss']
})
export class EtapaComponent implements OnInit {

  public etapas: Etapa[];

  constructor(private etapaService: EtapaService,
              private router: Router) {
    this.etapas = [];
  }

  ngOnInit() {
    this.getEtapasList();
  }

  editObra(etapa: Etapa) {
    this.router.navigate(['/etapa/edit', etapa.id]);
  }

  private getEtapasList() {
    this.etapaService.getEntities().subscribe(value => {
      this.etapas = value;
    });
  }

}
