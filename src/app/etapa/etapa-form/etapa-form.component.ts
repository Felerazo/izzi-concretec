import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Etapa} from '@app/shared/models/etapa';
import {EtapaService} from '@app/shared/service/etapa.service';

@Component({
  selector: 'app-etapa-form',
  templateUrl: './etapa-form.component.html',
  styleUrls: ['./etapa-form.component.scss']
})
export class EtapaFormComponent implements OnInit {

  etapa: Etapa;
  modeUpdate: boolean;
  constructor(private etapaService: EtapaService,
              private route: ActivatedRoute,
              private router: Router) {
    this.modeUpdate = false;
    this.etapa = new Etapa();
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.modeUpdate = true;
        console.log(params['id']);
        this.etapaService.getEntity(params['id']).subscribe(value => {
          this.etapa = value;
        });
      }
    });
  }

  onSubmit() {
    if (this.modeUpdate) {
      this.etapaService.updateEntity(this.etapa).subscribe(value => {
        this.router.navigate(['/etapa']);
      });
    }
    if (!this.modeUpdate) {
      this.etapaService.createEntity(this.etapa).subscribe(value => {
        this.router.navigate(['/etapa']);
      });
    }
  }

}
