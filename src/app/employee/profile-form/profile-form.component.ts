import {Component, OnInit} from '@angular/core';
import {ShareDataService} from '@app/shared/service/share-data.service';
import {ProfileEmployeeService} from '@app/shared/service/profile-employee.service';
import {Router} from '@angular/router';
import {Profile} from '@app/shared/models/profile';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})
export class ProfileFormComponent implements OnInit {

  NEXT_STEP = '/employee/contract/create';
  profile: Profile;
  modeUpdate: boolean;
  constructor(private shareDataService: ShareDataService,
              private router: Router,
              private  profileEmployeeService: ProfileEmployeeService) {
    this.profile = new Profile();
    this.modeUpdate = false;
  }

  ngOnInit() {
    if (this.shareDataService.getEmployee()) {
      this.profileEmployeeService.getProfileByEmployee(this.shareDataService.getEmployee().id).subscribe(value => {
        if (value.id) {
          this.profile = value;
          this.modeUpdate = true;
        }
      });
    }
    if (!this.shareDataService.getEmployee()) {
      this.router.navigate(['/employee']);
    }
  }

  onSubmit(): void {
    if (this.modeUpdate) {
      this.profile.employeeId = this.shareDataService.getEmployee().id;
      this.profileEmployeeService.updateEntity(this.profile).subscribe(value => {
        this.router.navigate([this.NEXT_STEP]);
      });
    }
    if (!this.modeUpdate) {
      this.profile.employeeId = this.shareDataService.getEmployee().id;
      this.profileEmployeeService.createEntity(this.profile).subscribe(value => {
        this.router.navigate([this.NEXT_STEP]);
      });
    }
  }

}
