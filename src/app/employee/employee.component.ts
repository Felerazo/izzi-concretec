import {Component, OnInit} from '@angular/core';
import {EmployeeService} from '@app/shared/service/employee.service';
import {Employee} from '@app/shared/models/employee';
import {Departament} from '@app/shared/models/departament';
import {Router} from '@angular/router';
import {ShareDataService} from '@app/shared/service/share-data.service';
import {DepartamentService} from '@app/shared/service/departament.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  public employees: Employee[];
  public departaments: Departament[];
  public isCollapsed: boolean;
  public departamentId: number;

  constructor(private employeeService: EmployeeService,
              private departamentService: DepartamentService,
              private shareDataService: ShareDataService,
              private router: Router) {
    this.employees = [];
    this.departaments = [];
    this.isCollapsed = true;
  }

  ngOnInit() {
    this.getEmployeesList();
  }

  editEmployee(employee: Employee) {
    this.shareDataService.setEmployeeSubs$(employee);
    this.router.navigate(['/employee/edit', employee.id]);
  }

  deleteEmployee(id: number) {
    this.employeeService.deleteEmployee(id).subscribe(value => {
      this.getEmployeesList();
    });
  }

  openCreateEmployee() {
    this.shareDataService.setEmployeeSubs$(null);
    this.router.navigate(['/employee/create']);
  }

  private getEmployeesList() {
    this.employeeService.getEmployees().subscribe(value => {
      this.employees = value;
    });
    this.departamentService.getEntities().subscribe(value => {
      this.departaments = value;
    });
  }

  private getEmployeesFilterList() {
    this.employeeService.getEmployeesFilter(this.departamentId).subscribe(value => {
      this.employees = value;
    });
    this.isCollapsed = true;
  }

  private addEmployeeObra(employee: Employee): void {
    this.shareDataService.setEmployeeSubs$(employee);
    this.router.navigate(['/employee/obra/create']);
  }

  private addEmployeeEquipo(employee: Employee): void {
    this.shareDataService.setEmployeeSubs$(employee);
    this.router.navigate(['/employee/equipo/create']);
  }
}
