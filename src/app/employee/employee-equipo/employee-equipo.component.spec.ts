import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeEquipoComponent } from './employee-equipo.component';

describe('EmployeeEquipoComponent', () => {
  let component: EmployeeEquipoComponent;
  let fixture: ComponentFixture<EmployeeEquipoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeEquipoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeEquipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
