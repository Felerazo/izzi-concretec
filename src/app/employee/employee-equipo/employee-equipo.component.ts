import { Component, OnInit } from '@angular/core';
import {EmloyeeEquipoService} from '@app/shared/service/emloyee-equipo.service';
import {ShareDataService} from '@app/shared/service/share-data.service';
import {Router} from '@angular/router';
import {EquipoService} from '@app/shared/service/equipo.service';
import {EmployeeEquipo} from '@app/shared/models/employee-equipo';
import {Equipo} from '@app/shared/models/equipo';

@Component({
  selector: 'app-employee-equipo',
  templateUrl: './employee-equipo.component.html',
  styleUrls: ['./employee-equipo.component.scss']
})
export class EmployeeEquipoComponent implements OnInit {

  employeeEquipo: EmployeeEquipo;
  employeeEquipos: EmployeeEquipo[];
  equipos: Equipo[];

  constructor(private shareDataService: ShareDataService,
              private equipoService: EquipoService,
              private employeeEquipoService: EmloyeeEquipoService,
              private router: Router) {
    this.employeeEquipo = new EmployeeEquipo();
    this.employeeEquipos = [];
    this.equipos = [];
  }

  ngOnInit() {
    if (!this.shareDataService.getEmployee()) {
      this.router.navigate(['/employee']);
    }
    if (this.shareDataService.getEmployee()) {
      this.getEquipos();
      this.getEmployeeEquipos();
    }
  }

  onSubmit(): void {
    this.employeeEquipo.employeeId = this.shareDataService.getEmployee().id;
    this.employeeEquipoService.createEntity(this.employeeEquipo).subscribe(value => {
      this.getEmployeeEquipos();
    });
  }

  deleteEmployeeObra(employeeEquipo: EmployeeEquipo): void {
    this.employeeEquipoService.deleteEntity(employeeEquipo.id).subscribe(value => {
      this.getEmployeeEquipos();
    });
  }

  private getEquipos(): void {
    this.equipoService.getEntities().subscribe(value => {
      this.equipos = value;
    });
  }

  private getEmployeeEquipos() {
    this.employeeEquipoService.getEmployeeEquipoByEmployee(this.shareDataService.getEmployee().id).subscribe(value => {
      this.employeeEquipos = value;
    });
  }

}
