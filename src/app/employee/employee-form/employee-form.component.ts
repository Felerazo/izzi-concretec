import {Component, OnInit} from '@angular/core';
import {DepartamentService} from '@app/shared/service/departament.service';
import {Departament} from '@app/shared/models/departament';
import {ActivatedRoute, Router} from '@angular/router';
import {Employee} from '@app/shared/models/employee';
import {EmployeeService} from '@app/shared/service/employee.service';
import {CargoService} from '@app/shared/service/cargo.service';
import {Cargo} from '@app/shared/models/cargo';
import {ShareDataService} from '@app/shared/service/share-data.service';
import {NgbDateAdapter, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateNativeAdapter} from '@app/shared/service/date-picker-adapter.service';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent implements OnInit {

  employee: Employee;
  modeUpdate: boolean;
  departaments: Departament[];
  cargos: Cargo[];

  constructor(private employeeService: EmployeeService,
              private departamentService: DepartamentService,
              private cargoService: CargoService,
              private shareDataService: ShareDataService,
              private route: ActivatedRoute,
              private router: Router) {
    this.modeUpdate = false;
    this.departaments = [];
    this.cargos = [];
    this.employee = new Employee();
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.modeUpdate = true;
        this.employeeService.getEmployee(params['id']).subscribe(value => {
          this.employee = value;
        });
      }
    });
    if (this.shareDataService.getEmployee()) {
      this.modeUpdate = true;
      this.employeeService.getEmployee(this.shareDataService.getEmployee().id).subscribe(value => {
        this.employee = value;
      });
    }
    this.departamentService.getEntities().subscribe(value => {
      this.departaments = value;
    });
    this.cargoService.getEntities().subscribe(value => {
      this.cargos = value;
    });
  }

  get today() {
    return new Date();
  }

  onSubmit() {
    this.employeeService.createEmployee(this.employee).subscribe(value => {
      this.shareDataService.setEmployeeSubs$(value);
      this.router.navigate(['/employee/profile/create']);
    });
  }
}
