import { Component, OnInit } from '@angular/core';
import {ShareDataService} from '@app/shared/service/share-data.service';
import {Router} from '@angular/router';
import {ContractService} from '@app/shared/service/contract.service';
import {Contract} from '@app/shared/models/contract';
import {TypeContractService} from '@app/shared/service/type-contract.service';
import {EmployeeService} from '@app/shared/service/employee.service';
import {TypeContract} from '@app/shared/models/type-contract';

@Component({
  selector: 'app-contract-form',
  templateUrl: './contract-form.component.html',
  styleUrls: ['./contract-form.component.scss']
})
export class ContractFormComponent implements OnInit {

  NEXT_STEP = '/employee';
  contract: Contract;
  modeUpdate: boolean;
  tipoContratos: TypeContract[];
  constructor(private shareDataService: ShareDataService,
              private contractService: ContractService,
              private typeContractService: TypeContractService,
              private router: Router) {
    this.contract = new Contract();
    this.modeUpdate = false;
  }

  ngOnInit() {
    if (this.shareDataService.getEmployee()) {
      this.contractService.getContractByEmployee(this.shareDataService.getEmployee().id).subscribe(value => {
        if (value.id) {
          this.contract = value;
          this.modeUpdate = true;
        }
      });
      this.typeContractService.getEntities().subscribe(value => {
        this.tipoContratos = value;
      });
    }
    if (!this.shareDataService.getEmployee()) {
      this.router.navigate(['/employee']);
    }
  }

  onSubmit(): void {
    if (this.modeUpdate) {
      this.contract.employeeId = this.shareDataService.getEmployee().id;
      this.contractService.updateEntity(this.contract).subscribe(value => {
        this.router.navigate([this.NEXT_STEP]);
      });
    }
    if (!this.modeUpdate) {
      this.contract.employeeId = this.shareDataService.getEmployee().id;
      this.contractService.createEntity(this.contract).subscribe(value => {
        this.router.navigate([this.NEXT_STEP]);
      });
    }
  }

}
