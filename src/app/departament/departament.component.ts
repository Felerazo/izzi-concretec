import {Component, OnInit} from '@angular/core';
import {DepartamentService} from '@app/shared/service/departament.service';
import {Departament} from '@app/shared/models/departament';
import {Router} from '@angular/router';

@Component({
  selector: 'app-departament',
  templateUrl: './departament.component.html',
  styleUrls: ['./departament.component.scss']
})
export class DepartamentComponent implements OnInit {

  public departamentos: Departament[];

  constructor(private departamentService: DepartamentService,
              private router: Router) {
    this.departamentos = [];
  }

  ngOnInit() {
    this.loadDepartaments();
  }
  editDepartament(departament: Departament) {
    this.router.navigate(['/departament/edit', departament.id]);
  }

  deleteDepartament(id: number) {
    this.departamentService.deleteEntity(id).subscribe(value => {
      this.loadDepartaments();
    });
  }

  private loadDepartaments() {
    this.departamentService.getEntities().subscribe(value => {
      this.departamentos = value;
    });
  }

}
