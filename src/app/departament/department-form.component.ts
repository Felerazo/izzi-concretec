import {Component, OnInit} from '@angular/core';
import {Departament} from '../shared/models/departament';
import {DepartamentService} from '../shared/service/departament.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-department-form',
  templateUrl: './department-form.component.html',
  styleUrls: ['./department-form.component.scss']
})
export class DepartmentFormComponent implements OnInit {

  modeUpdate: boolean;
  departament: Departament;

  constructor(private departamentService: DepartamentService,
              private route: ActivatedRoute,
              private router: Router) {
    this.modeUpdate = false;
    this.departament = new Departament();
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.modeUpdate = true;
        this.departamentService.getEntity(params['id']).subscribe(value => {
          this.departament = value;
        });
      }
    });
  }

  onSubmit() {
    if (this.modeUpdate) {
      this.departamentService.updateEntity(this.departament).subscribe(value => {
        this.router.navigate(['/departament']);
      });
    }
    if (!this.modeUpdate) {
      this.departamentService.createEntity(this.departament).subscribe(value => {
        this.router.navigate(['/departament']);
      });
    }
  }


}
