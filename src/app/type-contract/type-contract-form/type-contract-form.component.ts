import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TypeContractService} from '@app/shared/service/type-contract.service';
import {TypeContract} from '@app/shared/models/type-contract';

@Component({
  selector: 'app-type-contract-form',
  templateUrl: './type-contract-form.component.html',
  styleUrls: ['./type-contract-form.component.scss']
})
export class TypeContractFormComponent implements OnInit {

  modeUpdate: boolean;
  typeContract: TypeContract;

  constructor(private typeContractService: TypeContractService,
              private route: ActivatedRoute,
              private router: Router) {
    this.modeUpdate = false;
    this.typeContract = new TypeContract();
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.modeUpdate = true;
        console.log(params['id']);
        this.typeContractService.getEntity(params['id']).subscribe(value => {
          this.typeContract = value;
        });
      }
    });
  }

  onSubmit() {
    if (this.modeUpdate) {
      this.typeContractService.updateEntity(this.typeContract).subscribe(value => {
        this.router.navigate(['/typecontract']);
      });
    }
    if (!this.modeUpdate) {
      this.typeContractService.createEntity(this.typeContract).subscribe(value => {
        this.router.navigate(['/typecontract']);
      });
    }
  }

}
