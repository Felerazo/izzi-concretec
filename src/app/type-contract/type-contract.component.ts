import { Component, OnInit } from '@angular/core';
import {TypeContractService} from '@app/shared/service/type-contract.service';
import {Router} from '@angular/router';
import {TypeContract} from '@app/shared/models/type-contract';

@Component({
  selector: 'app-type-contract',
  templateUrl: './type-contract.component.html',
  styleUrls: ['./type-contract.component.scss']
})
export class TypeContractComponent implements OnInit {

  typeContracts: TypeContract[];
  constructor(private typeContractService: TypeContractService,
              private router: Router) {
    this.typeContracts = [];
  }

  ngOnInit() {
    this.typeContractService.getEntities().subscribe(value => {
      this.typeContracts = value;
    });
  }
  editTypeContract(typeContract: TypeContract) {
    this.router.navigate(['/typecontract/edit', typeContract.id]);
  }

}
