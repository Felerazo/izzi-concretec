import { Component, OnInit } from '@angular/core';
import {CargoService} from '@app/shared/service/cargo.service';
import {Cargo} from '@app/shared/models/cargo';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cargo',
  templateUrl: './cargo.component.html',
  styleUrls: ['./cargo.component.scss']
})
export class CargoComponent implements OnInit {

  cargos: Cargo[];
  constructor(private cargoService: CargoService,
              private router: Router) {
    this.cargos = [];
  }

  ngOnInit() {
    this.cargoService.getEntities().subscribe(value => {
      this.cargos = value;
    });
  }
  editCargo(cargo: Cargo) {
    this.router.navigate(['/cargo/edit', cargo.id]);
  }

}
