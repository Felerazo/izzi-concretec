import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Cargo} from '@app/shared/models/cargo';
import {CargoService} from '@app/shared/service/cargo.service';

@Component({
  selector: 'app-cargo-form',
  templateUrl: './cargo-form.component.html',
  styleUrls: ['./cargo-form.component.scss']
})
export class CargoFormComponent implements OnInit {

  modeUpdate: boolean;
  cargo: Cargo;

  constructor(private cargoService: CargoService,
              private route: ActivatedRoute,
              private router: Router) {
    this.modeUpdate = false;
    this.cargo = new Cargo();
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.modeUpdate = true;
        console.log(params['id']);
        this.cargoService.getEntity(params['id']).subscribe(value => {
          this.cargo = value;
        });
      }
    });
  }

  onSubmit() {
    if (this.modeUpdate) {
      this.cargoService.updateEntity(this.cargo).subscribe(value => {
        this.router.navigate(['/cargo']);
      });
    }
    if (!this.modeUpdate) {
      this.cargoService.createEntity(this.cargo).subscribe(value => {
        this.router.navigate(['/cargo']);
      });
    }
  }

}
