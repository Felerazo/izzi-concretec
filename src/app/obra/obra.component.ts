import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Obra} from '@app/shared/models/obra';
import {ObraService} from '@app/shared/service/obra.service';
import {ShareDataService} from '@app/shared/service/share-data.service';

@Component({
  selector: 'app-obra',
  templateUrl: './obra.component.html',
  styleUrls: ['./obra.component.scss']
})
export class ObraComponent implements OnInit {

  public obras: Obra[];
  public obra: Obra;
  public isCollapsed: boolean;

  constructor(private obrasService: ObraService,
              private shareDataService: ShareDataService,
              private router: Router) {
    this.obras = [];
    this.obra = new Obra();
    this.isCollapsed = true;
  }

  ngOnInit() {
    this.getObrasList();
  }

  editObra(obra: Obra) {
    this.router.navigate(['/obra/edit', obra.id]);
  }

  addObraEtapa(obra: Obra) {
    this.shareDataService.setObraSubs$(obra);
    this.router.navigate(['obra/etapa/create']);
  }

  public getObrasList() {
    this.obra = new Obra();
    this.obrasService.getEntities().subscribe(value => {
      this.obras = value;
    });
  }

  private getObrasFilter() {
    this.obrasService.getObrasFilter(this.obra).subscribe(value => {
      this.obras = value;
      this.isCollapsed = true;
    });
  }

}
