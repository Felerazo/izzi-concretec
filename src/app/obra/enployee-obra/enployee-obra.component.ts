import {Component, OnInit} from '@angular/core';
import {ShareDataService} from '@app/shared/service/share-data.service';
import {Router} from '@angular/router';
import {ObraService} from '@app/shared/service/obra.service';
import {EmployeeObraService} from '@app/shared/service/employee-obra.service';
import {EmployeeObra} from '@app/shared/models/employee-obra';
import {Obra} from '@app/shared/models/obra';

@Component({
  selector: 'app-enployee-obra',
  templateUrl: './enployee-obra.component.html',
  styleUrls: ['./enployee-obra.component.scss']
})
export class EnployeeObraComponent implements OnInit {

  employeeObra: EmployeeObra;
  employeeObras: EmployeeObra[];
  obras: Obra[];

  constructor(private shareDataService: ShareDataService,
              private obrasService: ObraService,
              private employeeObraService: EmployeeObraService,
              private router: Router) {
    this.employeeObra = new EmployeeObra();
    this.employeeObras = [];
    this.obras = [];
  }

  ngOnInit() {
    if (!this.shareDataService.getEmployee()) {
      this.router.navigate(['/employee']);
    }
    if (this.shareDataService.getEmployee()) {
      this.getObras();
      this.getEmployeeObras();
    }
  }

  onSubmit(): void {
    this.employeeObra.employeeId = this.shareDataService.getEmployee().id;
    this.employeeObraService.createEntity(this.employeeObra).subscribe(value => {
      this.getEmployeeObras();
    });
  }

  deleteEmployeeObra(employeeObra: EmployeeObra): void {
    this.employeeObraService.deleteEntity(employeeObra.id).subscribe(value => {
      this.getEmployeeObras();
    });
  }

  private getObras(): void {
    this.obrasService.getEntities().subscribe(value => {
      this.obras = value;
    });
  }

  private getEmployeeObras() {
    this.employeeObraService.getEmployeeObraByEmployee(this.shareDataService.getEmployee().id).subscribe(value => {
      this.employeeObras = value;
    });
  }
}
