import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnployeeObraComponent } from './enployee-obra.component';

describe('EnployeeObraComponent', () => {
  let component: EnployeeObraComponent;
  let fixture: ComponentFixture<EnployeeObraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnployeeObraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnployeeObraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
