import { Component, OnInit } from '@angular/core';
import {ShareDataService} from '@app/shared/service/share-data.service';
import {Router} from '@angular/router';
import {ObraEtapa} from '@app/shared/models/obra-etapa';
import {ObraEtapaService} from '@app/shared/service/obra-etapa.service';
import {EtapaService} from '@app/shared/service/etapa.service';
import {Etapa} from '@app/shared/models/etapa';

@Component({
  selector: 'app-obra-etapa',
  templateUrl: './obra-etapa.component.html',
  styleUrls: ['./obra-etapa.component.scss']
})
export class ObraEtapaComponent implements OnInit {

  obraEtapa: ObraEtapa;
  obraEtapas: ObraEtapa[];
  etapas: Etapa[];

  constructor(private shareDataService: ShareDataService,
              private obraEtapaService: ObraEtapaService,
              private etapaService: EtapaService,
              private router: Router) {
    this.obraEtapa = new ObraEtapa();
    this.obraEtapas = [];
    this.etapas = [];
  }

  ngOnInit() {
    if (!this.shareDataService.getObra()) {
      this.router.navigate(['/obra']);
    }
    if (this.shareDataService.getObra()) {
      this.getEtapas();
      this.getObraEtapas();
    }
  }

  onSubmit(): void {
    this.obraEtapa.obraId = this.shareDataService.getObra().id;
    this.obraEtapaService.createEntity(this.obraEtapa).subscribe(value => {
      this.getObraEtapas();
    });
  }

  deleteObraEtapa(obraEtapa: ObraEtapa): void {
    this.obraEtapaService.deleteEntity(obraEtapa.id).subscribe(value => {
      this.getObraEtapas();
    });
  }

  private getEtapas(): void {
    this.etapaService.getEntities().subscribe(value => {
      this.etapas = value;
    });
  }

  private getObraEtapas() {
    this.obraEtapaService.getObraEtapaByObra(this.shareDataService.getObra().id).subscribe(value => {
      this.obraEtapas = value;
    });
  }

}
