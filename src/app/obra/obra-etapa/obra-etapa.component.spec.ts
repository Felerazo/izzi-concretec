import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObraEtapaComponent } from './obra-etapa.component';

describe('ObraEtapaComponent', () => {
  let component: ObraEtapaComponent;
  let fixture: ComponentFixture<ObraEtapaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObraEtapaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObraEtapaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
