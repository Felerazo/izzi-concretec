import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AlmacenService} from '@app/shared/service/almacen.service';
import {Almacen} from '@app/shared/models/almacen';
import {Obra} from '@app/shared/models/obra';
import {ObraService} from '@app/shared/service/obra.service';

@Component({
  selector: 'app-obra-form',
  templateUrl: './obra-form.component.html',
  styleUrls: ['./obra-form.component.scss']
})
export class ObraFormComponent implements OnInit {

  obra: Obra;
  modeUpdate: boolean;

  constructor(private obraService: ObraService,
              private route: ActivatedRoute,
              private router: Router) {
    this.obra = new Obra();
    this.modeUpdate = false;
  }

  ngOnInit(): void {

    this.route.params.subscribe(params => {
      if (params['id']) {
        this.modeUpdate = true;
        this.obraService.getEntity(params['id']).subscribe(value => {
          this.modeUpdate = true;
          this.obra = value;
        });
      }
    });
  }

  onSubmit() {
    if (this.modeUpdate) {
      this.obraService.updateEntity(this.obra).subscribe(value => {
        this.router.navigate(['/obra']);
      });
    }
    if (!this.modeUpdate) {
      this.obraService.createEntity(this.obra).subscribe(value => {
        this.router.navigate(['/obra']);
      });
    }
  }

}
